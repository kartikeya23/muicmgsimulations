from muic_studies.constants import machines
from muic_studies.constants import model_commands

processes = [
    {'name': 'nc_cc_to_bb', 'title': 'VBF Higgs NC+CC to bb~',
     'commands': ['generate p mu- > mu- j h, h > b b~', 'generate p mu- > vm j h, h > b b~'], 'model': '5-heft'}
]

polarizations = [
    {'name': 'Pmu_eqm100', 'Pp': 0, 'Pmu': -100},
    {'name': 'Pmu_eqm40', 'Pp': 0, 'Pmu': -40},
    {'name': 'Pmu_eqm20', 'Pp': 0, 'Pmu': -20},
    {'name': 'Pmu_eqm10', 'Pp': 0, 'Pmu': -10},
    {'name': 'Pmu_eq0', 'Pp': 0, 'Pmu': 0},
    {'name': 'Pmu_eq10', 'Pp': 0, 'Pmu': 10},
    {'name': 'Pmu_eq20', 'Pp': 0, 'Pmu': 20},
    {'name': 'Pmu_eq40', 'Pp': 0, 'Pmu': 40},
    {'name': 'Pmu_eq100', 'Pp': 0, 'Pmu': 100},
]

base_config = {
    'pdlabel': 'lhapdf',
    'lhaid': 90000,
    'nevents': 50000,
    'lpp1': 0,
    'lpp2': 0,
    'ebeam1': 0,
    'ebeam2': 0,
    'polbeam1': 0,
    'polbeam2': 0,
    # DISABLE FUDICIAL CUTS
    'ptj': 0,
    'ptl': 0,
    'ptjmax': -1,
    'ptlmax': -1,
    'pt_min_pdg': '{}',
    'pt_max_pdg': '{}',
    'etaj': -1,
    'etal': -1,
    'etalmin': 0,
    'eta_min_pdg': '{}',
    'eta_max_pdg': '{}',
    'drjl': 0,
    'drjlmax': -1,
    'ptheavy': 0,
    'maxjetflavor': 5,
    'mmbb': 50,
    'mmbbmax': 170,
    'ptb': 20,
    'etab': 6,
    'etabmin': -6
}


def generate_process_generators(output_dir):
    # GENERATE PROCESSES
    for process in processes:
        print('Generating commands for %s' % process['title'])

        commands = list()

        # APPEND FLAVOR SUPPORT
        commands.extend(model_commands.get(process['model'], list()))

        # APPEND PROCESS
        commands.extend(process['commands'])

        # APPEND OUTPUT
        commands.append('output %s\n' % process['name'])

        # BUILD INPUT
        commands_output = ''
        for command in commands:
            commands_output += '%s%s' % ('' if commands_output == '' else '\n', command)

        with open('%s/generate_process_%s.txt' % (output_dir, process['name']), 'w') as f:
            f.write(commands_output)

        print('Done: %s\n' % process['name'])


def generate_runs(output_dir):
    process_names = [process['name'] for process in processes]

    for machine in machines:
        for process in process_names:
            print('Generating runs for machine %s and process %s' % (machine['name'], process))

            batch_name = '%s_%s' % (machine['name'], process)

            first = True

            commands_output = 'set automatic_html_opening False\n'

            for polarization in polarizations:
                run_name = '%s_%s' % (machine['name'], polarization['name'])

                if first:
                    first = False
                    commands_output += 'launch %s -n %s\n0\n' % (process, run_name)
                else:
                    commands_output += 'launch -n %s\n0\n' % run_name

                config = base_config.copy()

                config['lpp1'] = 1  # 0=NOPDF, 1=PROTON
                config['lpp2'] = 0  # 0=NOPDF, 1=PROTON
                config['ebeam1'] = machine['Ep']
                config['ebeam2'] = machine['Emu']
                config['polbeam1'] = polarization['Pp']
                config['polbeam2'] = polarization['Pmu']

                for key, value in config.items():
                    commands_output += 'set %s %s\n' % (key, value)

                commands_output += '0\n'

            with open('%s/run_%s.txt' % (output_dir, batch_name), 'w') as f:
                f.write(commands_output)

            print('Done: Machine %s Process: %s\n' % (machine['name'], process))


def generate_pythia_runs(output_dir):
    process_names = [process['name'] for process in processes]
    polarization_names = [polarization['name'] for polarization in polarizations]

    for machine in machines:
        for process in process_names:
            print('Generating pythia runs for machine %s and process %s' % (machine['name'], process))

            batch_name = '%s_%s' % (machine['name'], process)

            commands_output = 'set automatic_html_opening False\n'

            for polarization in polarization_names:
                run_name = '%s_%s' % (machine['name'], polarization)

                commands_output += 'pythia8 %s\n1\n0\n' % run_name

            with open('%s/run_pythia_%s.txt' % (output_dir, batch_name), 'w') as f:
                f.write(commands_output)

            print('Done: Machine %s Process: %s\n' % (machine['name'], process))
