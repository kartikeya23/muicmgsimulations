model_commands = {
    '5-sm': [
        'import model sm-no_b_mass'
    ],
    '5-heft': [
        'import model heft'
    ],
    '5-zp': [
        'import model zprime_UFO',
        'define p = p b b~',
        'define j = j b b~',
    ]
}

machines = [
    {'name': 'LHmuC', 'Emu': 1500, 'Ep': 7000},
    {'name': 'MuIC2', 'Emu': 1000, 'Ep': 1000},
    {'name': 'MuIC', 'Emu': 960, 'Ep': 275},
    {'name': 'HERA', 'Emu': 27.5, 'Ep': 920},
]
