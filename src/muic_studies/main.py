import argparse

from muic_studies.higgs import subcommand as higgs
from muic_studies.zprime import subcommand as zprime


def main():
    parser = argparse.ArgumentParser(description='MUIC Studies')
    subparsers = parser.add_subparsers(dest='parser', help='Studies')

    higgs.configure_parser(subparsers)
    zprime.configure_parser(subparsers)

    args = parser.parse_args()

    if args.parser == 'higgs':
        higgs.run(args)
    if args.parser == 'zprime':
        zprime.run(args)
