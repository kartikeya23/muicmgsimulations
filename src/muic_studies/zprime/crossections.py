from muic_studies.constants import machines
from muic_studies.constants import model_commands

processes = [
    {'name': 'bsm_zp_s_to_b', 'title': 'BSM Zprime b', 'commands': ['generate s mu- > mu- b'], 'model': '5-zp'},
    {'name': 'bsm_zp_b_to_s', 'title': 'BSM Zprime s', 'commands': ['generate b mu- > mu- s'], 'model': '5-zp'},
    {'name': 'nc_bj', 'title': 'NC to b', 'commands': ['generate p mu- > mu- b'], 'model': '5-sm'},
    {'name': 'nc_cj', 'title': 'NC to b', 'commands': ['generate p mu- > mu- c'], 'model': '5-sm'},
    {'name': 'nc_cbj', 'title': 'NC to b', 'commands': ['define j2 = c b', 'generate p mu- > mu- j2'], 'model': '5-sm'},
]

base_config = {
    'pdlabel': 'lhapdf',
    'lhaid': 90000,
    'nevents': 50000,
    'lpp1': 0,
    'lpp2': 0,
    'ebeam1': 0,
    'ebeam2': 0,
    'polbeam1': 0,
    'polbeam2': 0,
    # DISABLE FUDICIAL CUTS
    'ptj': 0,
    'ptl': 0,
    'ptjmax': -1,
    'ptlmax': -1,
    'pt_min_pdg': '{}',
    'pt_max_pdg': '{}',
    'etaj': -1,
    'etal': -1,
    'etalmin': 0,
    'eta_min_pdg': '{}',
    'eta_max_pdg': '{}',
    'drjl': 0,
    'drjlmax': -1,
    'ptheavy': 0,
    'maxjetflavor': 5,
}


def generate_process_generators(output_dir):
    # GENERATE PROCESSES
    for process in processes:
        print('Generating commands for %s' % process['title'])

        commands = list()

        # APPEND FLAVOR SUPPORT
        commands.extend(model_commands.get(process['model'], list()))

        # APPEND PROCESS
        commands.extend(process['commands'])

        # APPEND OUTPUT
        commands.append('output %s\n' % process['name'])

        # BUILD INPUT
        commands_output = ''
        for command in commands:
            commands_output += '%s%s' % ('' if commands_output == '' else '\n', command)

        with open('%s/generate_process_%s.txt' % (output_dir, process['name']), 'w') as f:
            f.write(commands_output)

        print('Done: %s\n' % process['name'])


def generate_zprime_mass_scan(output_dir):
    process_names = ['bsm_zp_s_to_b', 'bsm_zp_b_to_s']

    # MuIC Mass Scan
    for machine in [machines[2]]:
        for process in process_names:
            batch_name = '%s_%s' % (process, machine['name'])

            print('Generating %s' % batch_name)

            first = True

            commands_output = 'set automatic_html_opening False\n'

            masses = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

            for mass in masses:
                run_name = '%s_%sTeV' % (machine['name'], mass)

                if first:
                    first = False
                    commands_output += 'launch %s -n %s\n0\n' % (process, run_name)
                else:
                    commands_output += 'launch -n %s\n0\n' % run_name

                config = base_config.copy()
                config['nevents'] = 50000
                config['lpp1'] = 1  # 0=NOPDF, 1=PROTON
                config['lpp2'] = 0  # 0=NOPDF, 1=PROTON
                config['ebeam1'] = machine['Ep']
                config['ebeam2'] = machine['Emu']
                config['polbeam1'] = 0
                config['polbeam2'] = 0
                config['MZp'] = '%d' % (mass * 1000)  # TeV

                for key, value in config.items():
                    commands_output += 'set %s %s\n' % (key, value)

                commands_output += '0\n'

            with open('%s/run_%s.txt' % (output_dir, batch_name), 'w') as f:
                f.write(commands_output)

            print('Done: %s\n' % batch_name)


def generate_zprime_machine_comparison(output_dir):
    process_names = ['bsm_zp_s_to_b', 'bsm_zp_b_to_s']

    # Machine Comparison
    mass = 3  # TeV

    for process in process_names:
        batch_name = '%s_machine_comparison' % process

        print('Generating %s' % batch_name)

        first = True

        commands_output = 'set automatic_html_opening False\n'

        for machine in machines:
            run_name = '%s' % machine['name']

            if first:
                first = False
                commands_output += 'launch %s -n %s\n0\n' % (process, run_name)
            else:
                commands_output += 'launch -n %s\n0\n' % run_name

            config = base_config.copy()
            config['nevents'] = 50000
            config['lpp1'] = 1  # 0=NOPDF, 1=PROTON
            config['lpp2'] = 0  # 0=NOPDF, 1=PROTON
            config['ebeam1'] = machine['Ep']
            config['ebeam2'] = machine['Emu']
            config['polbeam1'] = 0
            config['polbeam2'] = 0
            config['MZp'] = '%d' % (mass * 1000)  # TeV

            for key, value in config.items():
                commands_output += 'set %s %s\n' % (key, value)

            commands_output += '0\n'

        with open('%s/run_%s.txt' % (output_dir, batch_name), 'w') as f:
            f.write(commands_output)

        print('Done: %s\n' % batch_name)


def generate_nc_runs(output_dir):
    process_names = ['nc_bj', 'nc_cj', 'nc_cbj']

    for machine in machines:
        for process in process_names:
            run_name = '%s_%s' % (process, machine['name'])

            print('Generating %s' % run_name)

            commands_output = 'set automatic_html_opening False\n'
            commands_output += 'launch %s -n %s\n0\n' % (process, machine['name'])

            config = base_config.copy()
            config['nevents'] = 1000000
            config['lpp1'] = 1  # 0=NOPDF, 1=PROTON
            config['lpp2'] = 0  # 0=NOPDF, 1=PROTON
            config['ebeam1'] = machine['Ep']
            config['ebeam2'] = machine['Emu']
            config['polbeam1'] = 0
            config['polbeam2'] = 0

            for key, value in config.items():
                commands_output += 'set %s %s\n' % (key, value)

            commands_output += '0\n'

            with open('%s/run_%s.txt' % (output_dir, run_name), 'w') as f:
                f.write(commands_output)

            print('Done: %s\n' % run_name)
