import os
import shutil

from commons.execution import runtime
from muic_studies.zprime import crossections


def configure_parser(subparsers):
    parser = subparsers.add_parser('zprime', help='Generates Zprime Studies MG5 Files')


def run(args):
    print('Generating Madgraph Diretory')
    os.makedirs('madgraph', exist_ok=True)

    print('Copying helper scripts')
    os.makedirs('madgraph/bin', exist_ok=True)
    shutil.copy(runtime.resource('madgraph/bin/exec_all_process_generators'), 'madgraph/bin')
    shutil.copy(runtime.resource('madgraph/bin/exec_all_runs'), 'madgraph/bin')
    shutil.copy(runtime.resource('madgraph/bin/collect_mg5_results'), 'madgraph/bin')

    print('Copying cards')
    os.makedirs('madgraph/cards', exist_ok=True)
    shutil.copy(runtime.resource('madgraph/cards/MuIC.tcl'), 'madgraph/cards')

    print('Generating process generation scripts')
    os.makedirs('madgraph/processes', exist_ok=True)
    crossections.generate_process_generators('madgraph/processes')

    print('Generating run scripts')
    os.makedirs('madgraph/runs', exist_ok=True)
    crossections.generate_zprime_mass_scan('madgraph/runs')
    crossections.generate_zprime_machine_comparison('madgraph/runs')
    crossections.generate_nc_runs('madgraph/runs')
