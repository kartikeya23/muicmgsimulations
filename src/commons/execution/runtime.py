import os

# GLOBAL
project_home = None
debug = False


def resource(path):
    return os.path.join(project_home, 'rsc', path)
